Ruzzle Solver
=============

![Rough sketch of a Ruzzle](/Zinob/ruzzle-solver/raw/master/russtleexample.png)

The goal of Ruzzle is to connect letters on a play-field to create words and this program was created after frequent complaints from friends that the stated number of solutions for a play-field was exaggerated.

Since there is 24000 words in even a rather modest swedish dictionary with the longest word being 29 characters and exhaustive search is un-feasable. In order to speed up the lookup a the n-ary trie data-structure was used. 

An example of the swedish words ack, adel, adla, ap, apel, ban, band, bann
![Rough sketch of a Ruzzle](/Zinob/ruzzle-solver/raw/master/trie.png)

A letter in parenthesis denotes a terminal.

The grid is traversed recursively in the directions North Northeast East Southeast South, Southwest, West,Northwest from every given node. The tree is traversed in parallel with a new root-node for the next search being the last searches end-node.

Every time the algorithm reaches a terminal letter the hierarchy so far is added to the list of finished words. If the next letter in a particular search is not found in the sub-tree the function returns and the next direction is tested, or if no more directions to search are available, the function also returns.

