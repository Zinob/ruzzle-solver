#!/usr/bin/python
"""A library routine to generate a trie from a flat-file"""


def mkDict(file='/usr/share/dict/words'):
	"""Returns a trie based on the flat-file wordlist specified by the file argument defaults to /usr/share/dict/words"""
	DictStruct={}
	f=open(file)
	for i in f.readlines():
		buildWord(i.strip().lower(),DictStruct)
	return DictStruct

def buildWord(word,dict):
	"""Recursivly traverses a word and add it to the suplied dictionary"""
	if len(word)==0:
		dict["END"]=1
		return
	letter=word[0]
	if dict.has_key(letter):
		buildWord(word[1:],dict[letter])
	else:
		dict[letter]={}
		buildWord(word[1:],dict[letter])

if __name__ == '__main__':
	mydic=mkDict()
	print mydic
	print "-"*10
	print mydic['h']['a']['n']['d']['END']


#Copyright (c) 2014 Simon Albinsson.
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#	 * Redistributions of source code must retain the above copyright
#		notice, this list of conditions and the following disclaimer.
#	 * Redistributions in binary form must reproduce the above copyright
#		notice, this list of conditions and the following disclaimer in the
#		documentation and/or other materials provided with the distribution.
#	 * Neither the name of Simon Albinsson nor the
#		names of its contributors may be used to endorse or promote products
#		derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL Simon Albinsson BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
